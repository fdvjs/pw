const crypto = require('crypto');
const { promisify } = require('util');

const randomBytes = promisify(crypto.randomBytes);
const pbkdf2 = promisify(crypto.pbkdf2);

const base64 = b => b.toString('base64');

class PasswordUtils {

    constructor ({ iters = 5000,
		   klen = 256,
		   digest = 'sha512' } = {}) {
	this.iters = iters;
	this.klen = klen;
	this.digest = digest;
    }
    
    async hash (pw, salt = null) {
	salt == null && (salt = await this.generateSalt());
	const hash = await pbkdf2(pw, salt,
				  this.iters,
				  this.klen,
				  this.digest).then(base64);
	return { hash, salt };
    }

    async verify (pw, hash, salt) {
	const test = await this.hash(pw, salt);
	return test.hash === hash;
    }

    async generateSalt (size = 16) {
	return await randomBytes(size).then(base64);
    }

}

pw.PasswordUtils = PasswordUtils;
pw.DEFAULT = new PasswordUtils();
function pw (config = null) {
    if (!config) { return pw.DEFAULT; }
    return new PasswordUtils(config);
}

module.exports = pw;
